# DFL Utils

The DesQ library to obtain system information and various classes and functions to be used across the DesQ project.
This library provides single-instance Application classes for Core and Gui, advanced file-system watcher, a very simple IPC,
functions to return XDG variables, desktop file parsing, and read various system info like battery, network, storage, cpu
and RAM info.


### Dependencies:
* <tt>Qt Core (qtbase5-dev, qtbase5-dev-tools)</tt>
* <tt>meson   (For configuring the project)</tt>
* <tt>ninja   (To build the project)</tt>


### Notes for compiling - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/desktop-frameworks/utils dfl-utils`
- Enter the `dfl-utils` folder
  * `cd dfl-utils`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release -Duse_qt_version=qt5`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Known Bugs
* Please test and let us know
