/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *
 * The DFL::Utils namespace contains a bunch of useful functions that
 * are used across the DFL Project. It also provides a rudimentary
 * logging functionality via qInfo/qDebug/qWarning/qCritical/qFatal.
 **/

#include <stdio.h>
#include <stdlib.h>

#include "DFUtils.hpp"

static QMimeDatabase mimeDb;
FILE                 *DFL::log = nullptr;

/**
 * Be default print only critical and fatal messages to the console.
 * To over-ride this behaviour without modifying the code, set the
 * environment variable DFL_DEBUG=1.
 */
bool DFL::SHOW_INFO_ON_CONSOLE     = false;
bool DFL::SHOW_DEBUG_ON_CONSOLE    = false;
bool DFL::SHOW_WARNING_ON_CONSOLE  = false;
bool DFL::SHOW_CRITICAL_ON_CONSOLE = true;
bool DFL::SHOW_FATAL_ON_CONSOLE    = true;

static const char *COLOR_INFO     = "\033[01;32m";
static const char *COLOR_DEBUG    = "\033[01;30m";
static const char *COLOR_WARN     = "\033[01;33m";
static const char *COLOR_CRITICAL = "\033[01;31m";
static const char *COLOR_FATAL    = "\033[01;41m";
static const char *COLOR_RESET    = "\033[00;00m";

void DFL::Logger( QtMsgType type, const QMessageLogContext& context, const QString& msg ) {
    /** Qt internal warning: Ignore it */
    if ( msg.startsWith( "Using QCharRef with an index pointing outside" ) ) {
        return;
    }

    QByteArray localMsg  = msg.toLocal8Bit();
    const char *file     = context.file ? context.file : "";
    const char *function = context.function ? context.function : "";

    switch ( type ) {
        case QtInfoMsg: {
            if ( DFL::SHOW_INFO_ON_CONSOLE || (qgetenv( "DFL_DEBUG" ) == "1") ) {
                fprintf(
                    stderr, "%s[I]: (%s:%u, %s) %s%s\n",
                    COLOR_INFO,
                    file, context.line, function,
                    localMsg.constData(),
                    COLOR_RESET
                );
                fflush( stderr );
            }

            if ( DFL::log ) {
                fprintf( DFL::log, "[I]: (%s:%u, %s) %s\n", file, context.line, function, localMsg.constData() );
                fflush( DFL::log );
            }

            break;
        }

        case QtDebugMsg: {
            if ( DFL::SHOW_DEBUG_ON_CONSOLE || (qgetenv( "DFL_DEBUG" ) == "1") ) {
                fprintf(
                    stderr, "%s[D]: (%s:%u, %s) %s%s\n",
                    COLOR_DEBUG,
                    file, context.line, function,
                    localMsg.constData(),
                    COLOR_RESET
                );
                fflush( stderr );
            }

            if ( DFL::log ) {
                fprintf( DFL::log, "[D]: (%s:%u, %s) %s\n", file, context.line, function, localMsg.constData() );
                fflush( DFL::log );
            }

            break;
        }

        case QtWarningMsg: {
            if ( DFL::SHOW_WARNING_ON_CONSOLE || (qgetenv( "DFL_DEBUG" ) == "1") ) {
                fprintf(
                    stderr, "%s[W]: (%s:%u, %s) %s%s\n",
                    COLOR_WARN,
                    file, context.line, function,
                    localMsg.constData(),
                    COLOR_RESET
                );
                fflush( stderr );
            }

            if ( DFL::log ) {
                fprintf( DFL::log, "[W]: (%s:%u, %s) %s\n", file, context.line, function, localMsg.constData() );
                fflush( DFL::log );
            }

            break;
        }

        case QtCriticalMsg: {
            if ( DFL::SHOW_CRITICAL_ON_CONSOLE || (qgetenv( "DFL_DEBUG" ) == "1") ) {
                fprintf(
                    stderr, "%s[E]: (%s:%u, %s) %s%s\n",
                    COLOR_CRITICAL,
                    file, context.line, function,
                    localMsg.constData(),
                    COLOR_RESET
                );
                fflush( stderr );
            }

            if ( DFL::log ) {
                fprintf( DFL::log, "[E]: (%s:%u, %s) %s\n", file, context.line, function, localMsg.constData() );
                fflush( DFL::log );
            }

            break;
        }

        case QtFatalMsg: {
            if ( DFL::SHOW_FATAL_ON_CONSOLE || (qgetenv( "DFL_DEBUG" ) == "1") ) {
                fprintf(
                    stderr, "%s[#]: (%s:%u, %s) %s%s\n",
                    COLOR_FATAL,
                    file, context.line, function,
                    localMsg.constData(),
                    COLOR_RESET
                );
                fflush( stderr );
            }

            if ( DFL::log ) {
                fprintf( DFL::log, "[#]: (%s:%u, %s) %s\n", file, context.line, function, localMsg.constData() );
                fflush( DFL::log );
            }

            break;
        }
    }
}


QString DFL::Utils::getMimeType( QString path ) {
    return mimeDb.mimeTypeForFile( path ).name();
}


QString DFL::Utils::getMimeIcon( QString path ) {
    return mimeDb.mimeTypeForFile( path ).iconName();
}


int DFL::Utils::mkpath( QString path, mode_t mode ) {
    /* Root always exists */
    if ( path == "/" ) {
        return 0;
    }

    QFileInfo fi( path );

    /* If the directory exists, thats okay for us */
    if ( fi.exists() ) {
        return 0;
    }

    mkpath( fi.path(), mode );

    return mkdir( path.toLocal8Bit().constData(), mode );
}


bool DFL::Utils::removeDir( QString dirName ) {
    bool result = true;
    QDir dir( dirName );

    QDirIterator it( dirName, QDir::NoDotAndDotDot | QDir::System | QDir::Hidden | QDir::AllDirs | QDir::Files );

    while ( it.hasNext() ) {
        it.next();

        if ( it.fileInfo().isDir() ) {
            result &= DFL::Utils::removeDir( it.filePath() );
        }

        else {
            result &= QFile::remove( it.filePath() );
        }
    }

    rmdir( dirName.toUtf8().constData() );

    return result;
}


bool DFL::Utils::isReadable( QString path ) {
    QFileInfo info( path );

    if ( info.isDir() ) {
        return (info.isReadable() and info.isExecutable() );
    }

    else {
        return info.isReadable();
    }
}


bool DFL::Utils::isWritable( QString path ) {
    return not access( path.toLocal8Bit().constData(), W_OK );
}


bool DFL::Utils::isExecutable( QString path ) {
    struct stat statbuf;

    if ( stat( path.toLocal8Bit().data(), &statbuf ) != 0 ) {
        return false;
    }

    if ( (statbuf.st_mode & S_IXUSR) ) {
        QMimeType m = mimeDb.mimeTypeForFile( path );

        if ( m.name() == "application/x-executable" ) {
            return true;
        }

        else if ( m.name() == "application/x-sharedlib" ) {
            return true;
        }

        else if ( m.allAncestors().contains( "application/x-executable" ) ) {
            return true;
        }

        /* Default is false */
        return false;
    }

    return false;
}


mode_t DFL::Utils::getMode( QString path ) {
    struct stat fileAtts;

    if ( stat( path.toLocal8Bit().constData(), &fileAtts ) != 0 ) {
        return -1;
    }

    return fileAtts.st_mode;
}


QStringList DFL::Utils::recDirWalk( QString path ) {
    QStringList fileList;

    if ( not QFileInfo( path ).exists() ) {
        return QStringList();
    }

    QDirIterator it( path, QDir::AllEntries | QDir::System | QDir::NoDotAndDotDot | QDir::Hidden, QDirIterator::Subdirectories );

    while ( it.hasNext() ) {
        it.next();

        if ( it.fileInfo().isDir() ) {
            fileList.append( it.fileInfo().filePath() );
        }

        else {
            fileList.append( it.fileInfo().filePath() );
        }
    }

    return fileList;
}


QString DFL::Utils::formatSize( qint64 size, qint64 hint ) {
    if ( hint == SizeHint::Auto ) {
        if ( size >= SizeHint::TiB ) {
            return QString( "%1 TiB" ).arg( QString::number( qreal( size ) / SizeHint::TiB, 'f', 3 ) );
        }
        else if ( size >= SizeHint::GiB ) {
            return QString( "%1 GiB" ).arg( QString::number( qreal( size ) / SizeHint::GiB, 'f', 2 ) );
        }
        else if ( size >= SizeHint::MiB ) {
            return QString( "%1 MiB" ).arg( QString::number( qreal( size ) / SizeHint::MiB, 'f', 1 ) );
        }
        else if ( size >= SizeHint::KiB ) {
            return QString( "%1 KiB" ).arg( QString::number( qreal( size ) / SizeHint::KiB, 'f', 1 ) );
        }
        else {
            return QString( "%1 B" ).arg( size );
        }
    }

    else {
        if ( hint == SizeHint::TiB ) {
            return QString( "%1 TiB" ).arg( QString::number( qreal( size ) / SizeHint::TiB, 'f', 3 ) );
        }
        else if ( hint == SizeHint::GiB ) {
            return QString( "%1 GiB" ).arg( QString::number( qreal( size ) / SizeHint::GiB, 'f', 2 ) );
        }
        else if ( hint == SizeHint::MiB ) {
            return QString( "%1 MiB" ).arg( QString::number( qreal( size ) / SizeHint::MiB, 'f', 1 ) );
        }
        else if ( hint == SizeHint::KiB ) {
            return QString( "%1 KiB" ).arg( QString::number( qreal( size ) / SizeHint::KiB, 'f', 1 ) );
        }
        else {
            return QString( "%1 B" ).arg( size );
        }
    }

    return QString();
}


double DFL::Utils::formatSizeRaw( qint64 size, qint64 hint ) {
    if ( hint == SizeHint::Auto ) {
        if ( size >= SizeHint::TiB ) {
            return qreal( size ) / SizeHint::TiB;
        }
        else if ( size >= SizeHint::GiB ) {
            return qreal( size ) / SizeHint::GiB;
        }
        else if ( size >= SizeHint::MiB ) {
            return qreal( size ) / SizeHint::MiB;
        }
        else if ( size >= SizeHint::KiB ) {
            return qreal( size ) / SizeHint::KiB;
        }
        else {
            return size;
        }
    }

    else {
        if ( hint == SizeHint::TiB ) {
            return qreal( size ) / SizeHint::TiB;
        }
        else if ( hint == SizeHint::GiB ) {
            return qreal( size ) / SizeHint::GiB;
        }
        else if ( hint == SizeHint::MiB ) {
            return qreal( size ) / SizeHint::MiB;
        }
        else if ( hint == SizeHint::KiB ) {
            return qreal( size ) / SizeHint::KiB;
        }
        else {
            return size;
        }
    }

    return size;
}


QString DFL::Utils::formatSizeStr( qint64 size, qint64 hint ) {
    if ( hint == SizeHint::Auto ) {
        if ( size >= SizeHint::TiB ) {
            return QString( "TiB" );
        }
        else if ( size >= SizeHint::GiB ) {
            return QString( "GiB" );
        }
        else if ( size >= SizeHint::MiB ) {
            return QString( "MiB" );
        }
        else if ( size >= SizeHint::KiB ) {
            return QString( "KiB" );
        }
        else {
            return QString( "B" );
        }
    }

    else {
        if ( hint == SizeHint::TiB ) {
            return QString( "TiB" );
        }
        else if ( hint == SizeHint::GiB ) {
            return QString( "GiB" );
        }
        else if ( hint == SizeHint::MiB ) {
            return QString( "MiB" );
        }
        else if ( hint == SizeHint::KiB ) {
            return QString( "KiB" );
        }
        else {
            return QString( "B" );
        }
    }

    return "B";
}
