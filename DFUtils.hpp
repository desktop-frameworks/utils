/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *
 * The DFL::Utils namespace contains a bunch of useful functions that
 * are used across the DFL Project. It also provides a rudimentary
 * logging functionality via qInfo/qDebug/qWarning/qCritical/qFatal.
 **/

#pragma once

// Qt Headers
#include <QtCore>
#include <libgen.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <QMimeDatabase>

namespace DFL {
    extern FILE *log;
    extern bool SHOW_INFO_ON_CONSOLE;
    extern bool SHOW_DEBUG_ON_CONSOLE;
    extern bool SHOW_WARNING_ON_CONSOLE;
    extern bool SHOW_CRITICAL_ON_CONSOLE;
    extern bool SHOW_FATAL_ON_CONSOLE;

    void Logger( QtMsgType type, const QMessageLogContext& context, const QString& message );

    namespace Utils {
        namespace SizeHint {
            const qint64 Auto = 0;
            const qint64 KiB  = 1024;
            const qint64 MiB  = 1024 * KiB;
            const qint64 GiB  = 1024 * MiB;
            const qint64 TiB  = 1024 * GiB;
        }

        /* MimeType and MimeIcon from QMimeDatabase */
        QString getMimeType( QString path );
        QString getMimeIcon( QString path );

        /*
         * Create a directory: mkdir -p
         * http://fossies.org/linux/inadyn/libite/makepath.c
         */
        int mkpath( QString, mode_t mode = 0755 );

        /* Remove a directory and its contents */
        bool removeDir( QString );

        /* Is the path readable */
        bool isReadable( QString path );
        bool isWritable( QString path );
        bool isExecutable( QString path );

        /* Mode of a file/folder */
        mode_t getMode( QString path );

        /* Get all the files and subdirs in directory */
        QStringList recDirWalk( QString );

        /* Convert the numberic size to human readable string */
        QString formatSize( qint64, qint64 hint    = SizeHint::Auto );
        QString formatSizeStr( qint64, qint64 hint = SizeHint::Auto );
        double  formatSizeRaw( qint64, qint64 hint = SizeHint::Auto );
    }
}
